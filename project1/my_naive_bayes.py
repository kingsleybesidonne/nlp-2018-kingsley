#!/usr/bin/env python
# coding: utf-8

# In[4]:


import nltk
import math
import numpy as np
from string import punctuation
import operator
import json 


class NaiveBayes:
    def __init__(self):
        self.classes = ["0", "1"]  # hold the two discrete classes
        self.status = False 
        self.wordDic = {0: [], 1: []}  # holds all the positive and negative words
        self.testDataList = []  # holds a list of testing data
        self.trainDataList = []  # holds a list of training data
        self.testDic = {"data": [], "class": []} # holds the test data and the classes
        self.sum = {}  # holds the sum of all the prior and likelihoods of the discrete classes
        self.positiveDoc = 0           # holds the count of all positive documents
        self.negativeDoc = 0           # holds the count of all negative documents
        self.vocabulary = []           # holds all the words in both discrete class
        self.prior = {}                # holds the prior of both the positive and negative class
        self.likelihood = {"0": {}, "1": {}}  # holds the likelihood of each word for each class

    def read(self, file):  # reads in the data set
        dataList = []
        for line in file.readlines():
            data = line.strip("\n").split("\t")
            dataList.append(data)
        self.dataSplit(dataList)
        self.dataEdit(self.testDataList, self.trainDataList)

    def read_test_set(self, file):  # predicts the sentences in a file and returns a file with predictions
        testList = []
        for line in file.readlines():
            testList.append(line)
        results = self.predict(testList)
        
        file_result = open("results.txt", "w")
        for result in results:
            file_result.write(result+"\n")
        file_result.close()
        print("successful")

    def test_classifier(self):  # tests the accuracy of the classifier
        correct = 0
        result = self.predict(self.testDic["data"])
        for i in range(len(result)):
            if str(result[i]) == self.testDic["class"][i]:
                correct = correct + 1

        accuracy = (correct / len(self.testDic["data"])) * 100

        print("test document : ")
        print(len(self.testDic["data"]))
        print("accuracy: ")
        print(accuracy)
        return accuracy

    def train(self):  # trains the classifier with training data set
        totalDoc = len(self.trainDataList)  # the total distinct words in the vocabulary
        positive = math.log((self.positiveDoc/totalDoc), 10)     # prior for the positive class
        negative = math.log((self.negativeDoc/totalDoc), 10)    # prior for the negative class

        self.prior["1"] = positive
        self.prior["0"] = negative

        for word in set(self.vocabulary):
            self.likelihood["1"][word] = math.log((self.wordDic[1].count(word) + 1) / 
                                                (self.positiveDoc + len(set(self.vocabulary))), 10)
            self.likelihood["0"][word] = math.log((self.wordDic[0].count(word) + 1) / 
                                                (self.negativeDoc + len(set(self.vocabulary))), 10)

        self.read_to_json()
        self.status = True
        return self.status

    def read_to_json(self):
        with open("likelihood.txt", "w") as outfile:
            json.dump(self.likelihood, outfile)

        with open("prior.txt", "w") as file:
            json.dump(self.prior, file)

        with open("vocabulary.txt", "w") as vocab_file:
            json.dump(self.vocabulary, vocab_file)

    def read_from_json(self):
        with open("likelihood.txt") as json_file:
            self.likelihood = json.load(json_file)
        with open("prior.txt") as json_file1:
            self.prior = json.load(json_file1)
        with open("vocabulary.txt") as json_file2:
            self.vocabulary = json.load(json_file2)

    def test(self, sentence):  # test a sentence and returns a prediction
        if self.prior == {}:
            self.read_from_json()

        for i in range(len(self.classes)):
            self.sum[self.classes[i]] = self.prior[self.classes[i]]
            test_word = sentence.split(" ")
            for j in range(len(test_word)):
                word = test_word[j].lower()
                if word in self.vocabulary:
                    self.sum[self.classes[i]] = self.sum[self.classes[i]] + self.likelihood[self.classes[i]][word]

        return max(self.sum.items(), key=operator.itemgetter(1))[0]

    def predict(self, sentenceList):  # predicts the class of sentences in a list and returns a list of predictions
        result = []
        for i in range(len(sentenceList)):
            result.append(self.test(sentenceList[i]))
        return result

    def dataSplit(self, wordList):  # splits the data set into training data and test data
        np.random.shuffle(wordList)
        testDataCount = int(0.2 * len(wordList))
        self.testDataList += wordList[0:testDataCount]
        self.trainDataList += wordList[testDataCount:]

    def dataEdit(self, testList, trainList):  # edits the training and test data set
        char = set(list(punctuation))
        for i in range(len(testList)):
            self.testDic["data"].append(testList[i][0])
            self.testDic["class"].append(testList[i][1])

        for i in range(len(trainList)):
            if trainList[i][1] == "0":
                self.negativeDoc = self.negativeDoc + 1
                sentence = trainList[i][0]
                tokens = nltk.word_tokenize(sentence)
                for word in tokens:
                    if word not in char:
                        self.vocabulary.append(word.lower())
                        self.wordDic[0].append(word.lower())

            else:
                self.positiveDoc = self.positiveDoc + 1
                sentence = trainList[i][0]
                tokens = nltk.word_tokenize(sentence)
                for word in tokens:
                    if word not in char:
                        self.vocabulary.append(word.lower())
                        self.wordDic[1].append(word.lower())


# In[6]:


def main():
    user_input = input("Test File: ")
    naive = NaiveBayes()
    with open(user_input, "r") as test_file:
        naive.read_test_set(test_file)


main()


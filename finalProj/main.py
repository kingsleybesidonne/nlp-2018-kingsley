#!/usr/bin/env python
# coding: utf-8

# In[21]:


import pandas as pd
import nltk 
import json
import sys 
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import roc_auc_score
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn import preprocessing 
from sklearn.preprocessing import LabelEncoder
from sklearn import metrics
from sklearn.preprocessing import scale 


# In[47]:


class NB:
    def __init__(self):
        self.clf = MultinomialNB() #This variable is for our classifier 
        self.X_train = None #This variable is for our train data sentences
        self.X_test = None #This variable is for our test data sentences
        self.y_train = None #This variable is for our train data predictions 
        self.y_test = None #This variable is for our test data predictions 
        self.X_train_tfidf = None #This variable is for our transformed training data sentences 
        self.X_test_tfidf = None 
        self.count_vect = None 
        
    #This function reads in the data from a file and splits it into training and test data    
    def read(self,file, file2):
        df = pd.read_csv(file, sep='\t', names=['questions'])
        df2 = pd.read_csv(file2, sep='\t', names=['topics'])
        df['topics'] = df2.topics
        df['topics_id'] = df2['topics'].factorize()[0]
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(df['questions'], df['topics'],random_state=0)
        self.count_vect = CountVectorizer()
        X_train_counts = self.count_vect.fit_transform(self.X_train)
        self.X_test_tfidf = self.count_vect.transform(self.X_test)
        tfidf_transformer = TfidfTransformer()
        self.X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
        #self.X_test_tfidf = tfidf_transformer.transform(X_test_counts)
        
    #This function trains the classifier with the training data
    def train(self):
        clf = self.clf.fit(self.X_train_tfidf, self.y_train)
    
    def predict_line(self,sentence):
        print(self.clf.predict(self.count_vect.transform([sentence])))
        
    #This function predicts the test file and writes the results to a file named results.txt    
    def predict(self,file):
        testList = []
        for line in file.readlines():
            testList.append(line)
        test_list_vector = self.count_vect.transform(testList)
        results = self.clf.predict(test_list_vector)
        result_list = list(results)
        file_result = open("topic_results.txt","w")
        for result in result_list:
            file_result.write(str(result)+"\n")
        file_result.close()
        
    #This function checks the accuracy of the classifier     
    def accuracy(self):
        accuracy = self.clf.score(self.X_test_tfidf,self.y_test)
        print (accuracy)  
        


# In[49]:


class LG:
    def __init__(self):
        self.clf = LogisticRegression() #This variable is for our classifier 
        self.X_train = None #This variable is for our train data sentences 
        self.X_test = None #This variable is for our test data sentences 
        self.y_train = None #This variable is for our train data predictions 
        self.y_test = None #This varible is for our test data  predictions 
        self.vector = None 
        
    #This function reads in the data from a file and splits it into training and test data    
    def read(self,file, file2):
        df = pd.read_csv(file, sep='\t', names=['questions'])
        df2 = pd.read_csv(file2, sep='\t', names=['topics'])
        df['topics'] = df2.topics
        df['topics_id'] = df2['topics'].factorize()[0]
        stopword_set = set(stopwords.words('english'))
        self.vector = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='latin-1', ngram_range=(1, 2), stop_words='english')
        data_predict = df.topics
        data_txt = self.vector.fit_transform(df.questions).toarray()
        data_txt.shape
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(data_txt,data_predict,test_size=0.33,random_state=0)
        
    #This function trains the classifier with the training data
    def train(self):
        clf = self.clf.fit(self.X_train, self.y_train)
        
    #This function predicts the test file and writes the results to a file named results.txt   
    def predict(self,file):
        testList = []
        for line in file.readlines():
            testList.append(line)
        test_list_vector = self.vector.transform(testList)
        results = self.classifier.predict(test_list_vector)
        result_list = list(results)
        file_result = open("topic_results.txt","w")
        for result in result_list:
            file_result.write(str(result)+"\n")
        file_result.close()
        
    #This function checks the accuracy of the classifier     
    def accuracy(self):
        accuracy = self.clf.score(self.X_test,self.y_test)
        print (accuracy)
        
        


# In[ ]:


def main():
    name = sys.argv[1]
    file = sys.argv[2]
    bayes = NB()
    bayes.read('questions.txt','topics.txt')
    bayes.train()
    bayes.predict(file)

main()


# In[48]:


bayes = NB()
bayes.read('questions.txt','topics.txt')
bayes.train()
bayes.accuracy()


# In[50]:


log = LG()
log.read('questions.txt','topics.txt')
log.train()
log.accuracy()


# In[131]:


log.predict('how do I play the piano')


# In[46]:


bayes.predict_line("where is Ghana ?")


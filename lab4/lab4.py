#!/usr/bin/env python
# coding: utf-8

# In[107]:


import pandas as pd
import nltk 
import sys 
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn import naive_bayes
from sklearn.metrics import roc_auc_score
from sklearn.linear_model import LogisticRegression 



# In[108]:


#This is the class for the normalised naive bayes classifier
class NaiveBayes: 
    def __init__(self):
        self.data_txt_train = None #this is the variable for our train data sentences
        self.data_txt_test = None #this is the variable for our test data sentences
        self.data_predict_train = None #this is the variable for our train data predictions 
        self.data_predict_test = None #this id the variable for our test data predictions 
        self.classifier = naive_bayes.MultinomialNB() #this is the variable for our classifier 
        self.trained = None  #this is the variable for our trained classifier 
        self.vector = None 
        
        
    #This function reads in the data from a file and splits it into training and test data 
    def read(self,file):
        df = pd.read_csv(file, sep='\t', names=['txt','prediction']) #This reads in the data 
        stopword_set = set(stopwords.words('english')) #This creates a set of stopwords in english 
        self.vector = TfidfVectorizer(use_idf=True , lowercase=True , strip_accents=ascii , stop_words=stopword_set)
        data_predict = df.prediction #This assigns the predictions to the a variable 
        data_txt = self.vector.fit_transform(df.txt) 
        self.data_txt_train, self.data_txt_test, self.data_predict_train,self.data_predict_test = train_test_split(data_txt,data_predict) #This splits our data into training and test set and assigns it to 
        print("successful")
    

    
    #This function trains the classifier with the training data 
    def train(self):
        self.trained = self.classifier.fit(self.data_txt_train,self.data_predict_train)
        
        
    #This function checks the accuracy of the classifier 
    def accuracy(self):
        accuracy = roc_auc_score(self.data_predict_test, self.classifier.predict_proba(self.data_txt_test)[:,1])
   
        
        
    #This function predicts the class of a line and prints out the output 
    def predict_line(self, string):
        testList = []
        testList.append(string)
        test_list = self.vector.transform(testList)
        result = self.classifier.predict(test_list)

        
        
        
    #This function predicts the test file and writes the results to a file named results.txt
    def predict(self,file):          
        testList = []
        for line in file.readlines():
            testList.append(line)
        test_list_vector = self.vector.transform(testList)
        results = self.classifier.predict(test_list_vector)
        result_list = list(results)
        file_result = open("nb-results.txt","w")
        for result in result_list:
            file_result.write(str(result)+"\n")
        file_result.close()
        print("successful")
        
        
        


# In[109]:

#This is the class for the unnormalised naive bayes
class NaiveBayes_U:
    def __init__(self):
        self.data_txt_train = None #This is the varibale for our train data sentences
        self.data_txt_test = None #This  is the variable for our test data sentences 
        self.data_predict_train = None #This is the variable for our train data predictions 
        self.data_predict_test = None #This is the variable for our test data predictions 
        self.classifier = naive_bayes.MultinomialNB() #Initialization of the naive bayes classifier 
        self.trained = None #This is the variable for our trained classifier 
        self.vector = None #This is the varible for the Vectorizer
        
        
    #This function reads in data from a file and splits it into training and test data 
    def read(self,file):
        df = pd.read_csv(file, sep='\t', names=['txt','prediction'])
        self.vector = TfidfVectorizer()
        data_predict = df.prediction
        data_txt = self.vector.fit_transform(df.txt)
        self.data_txt_train, self.data_txt_test, self.data_predict_train, self.data_predict_test = train_test_split(data_txt,data_predict)
        print("successful")
    

    
    #This function trains the classifier 
    def train(self):
        self.trained = self.classifier.fit(self.data_txt_train,self.data_predict_train)
        
        

    #This function checks the accuracy of the classifier 
    def accuracy(self):
        accuracy = roc_auc_score(self.data_predict_test, self.classifier.predict_proba(self.data_txt_test)[:,1])
  

        
        
    #This function predicts a data from a test file and outputs the result to a file name results.txt 
    def predict(self,file):          
        testList = []
        for line in file.readlines():
            testList.append(line)
        test_list_vector = self.vector.transform(testList)
        results = self.classifier.predict(test_list_vector)
        result_list = list(results)
        file_result = open("nb-u-results.txt","w")
        for result in result_list:
            file_result.write(str(result)+"\n")
        file_result.close()
        print("successful")
        
        
        


# In[110]:

#This is the class for the unnormalised Logistic Regression
class LogisticReg_U:
    def __init__(self):
        self.classifier = LogisticRegression() #Initialization of the Logistic Regression Classifier 
        self.data_txt_train = None #This is the varibale for our train data sentences 
        self.data_txt_test = None #This is the varible for our test data sentences 
        self.data_predict_train = None  #This is the variable for our train data predictions 
        self.data_predict_test = None  #This is the varible for our test data predictions 
        self.trained = None  #This is the variable for our trained classifier 
        self.vector = None #This is the variable for the Vectorizer
        
        
     #This function reads the data and splits it into training and test data   
    def read(self, file):
        df = pd.read_csv(file , sep='\t' , names=['txt','prediction'])
        self.vector = TfidfVectorizer()
        data_predict = df.prediction
        data_txt = self.vector.fit_transform(df.txt)
        self.data_txt_train, self.data_txt_test, self.data_predict_train, self.data_predict_test = train_test_split(data_txt,data_predict)
        print("successful")
        
        
     #This function trains the classifier    
    def train(self):
        self.trained = self.classifier.fit(self.data_txt_train, self.data_predict_train)
        
    #This function checks the accuracy of the classifier 
    def accuracy(self):
        accuracy = roc_auc_score(self.data_predict_test, self.classifier.predict_proba(self.data_txt_test)[:,1])

        
    #This predicts a test file and outputs the result to a file 
    def predict(self,file):          
        testList = []
        for line in file.readlines():
            testList.append(line)
        test_list_vector = self.vector.transform(testList)
        results = self.classifier.predict(test_list_vector)
        result_list = list(results)
        file_result = open("lr-u-results.txt","w")
        for result in result_list:
            file_result.write(str(result)+"\n")
        file_result.close()
        print("successful")


# In[111]:

#This is the class for the normalised Logistic regression 
class LogisticReg:
    def __init__(self):
        self.classifier = LogisticRegression()#Initializes the Logistic Regression classifier 
        self.data_txt_train = None #This is the variable for the train data sentences 
        self.data_txt_test = None #This is the varible for the test data sentences 
        self.data_predict_train = None #This is the variable for the train predictions 
        self.data_predict_test = None #This is the varible for the test predictions 
        self.trained = None #This is the variable for the trained classifier 
        self.vector = None #This is the variable for the Vectorizer 
        
        
        
     #This function reads the data and splits it into training and test data    
    def read(self, file):
        df = pd.read_csv(file , sep='\t' , names=['txt','prediction'])
        stopword_set = set(stopwords.words('english'))
        self.vector = TfidfVectorizer(use_idf=True , lowercase=True , strip_accents=ascii , stop_words=stopword_set)
        data_predict = df.prediction
        data_txt = self.vector.fit_transform(df.txt)
        self.data_txt_train, self.data_txt_test, self.data_predict_train, self.data_predict_test = train_test_split(data_txt,data_predict)
        print("successful")
        
        
       #This function trains the classifier  
    def train(self):
        self.trained = self.classifier.fit(self.data_txt_train, self.data_predict_train)
        
        
        #This function checks the accuracy of the classifier 
    def accuracy(self):
        accuracy = roc_auc_score(self.data_predict_test, self.classifier.predict_proba(self.data_txt_test)[:,1])
        print(accuracy)
        
        #This function predicts a test file and outputs the result to a file 
    def predict(self,file):          
        testList = []
        for line in file.readlines():
            testList.append(line)
        test_list_vector = self.vector.transform(testList)
        results = self.classifier.predict(test_list_vector)
        result_list = list(results)
        file_result = open("lr-results.txt","w")
        for result in result_list:
            file_result.write(str(result)+"\n")
        file_result.close()
        print("successful")


if __name__=='__main__':
    def main():
        name = sys.argv[1]
        version = sys.argv[2]
        file = sys.argv[3]
        if(name == "nb"):
            if(version == "n"):
                naive_normalised = NaiveBayes()
                naive_normalised.read("trainingSet.txt")
                naive_normalised.train()
                with open(file,"r") as test_file:
                    naive_normalised.predict(test_file)
                    
            elif(version == "u"):
                naive_unnormalised = NaiveBayes_U()
                naive_unnormalised.read("trainingSet.txt")
                naive_unnormalised.train()
                with open(file,"r") as test_file:
                    naive_unnormalised.predict(test_file)
                    
        elif(name == "lr"):
            if(version == "n"):
                log_normalised = LogisticReg()
                log_normalised.read("trainingSet.txt")
                log_normalised.train()
                with open(file,"r") as test_file:
                    log_normalised.predict(test_file)
                    
            elif(version == "u"):
                log_unnormalised = LogisticReg_U()
                log_unnormalised.read("trainingSet.txt")
                log_unnormalised.train()
                with open(file,"r") as test_file:
                    log_unnormalised.predict(test_file)


main()





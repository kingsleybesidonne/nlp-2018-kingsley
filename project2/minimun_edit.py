import numpy as np

def min_edit_distance(source, target):
    source_length = len(source)  # length of the source
    target_length = len(target)  # length of the target

    dist_matrix = np.zeros((source_length + 1, target_length + 1), dtype=np.int)  # distance matrix

    for i in range(1, source_length + 1):
        dist_matrix[i][0] = dist_matrix[i-1][0] + 1

    for j in range(1, target_length + 1):
        dist_matrix[0][j] = dist_matrix[0][j-1] + 1

    for i in range(1, source_length + 1):
        for j in range(1, target_length + 1):
            dist_matrix[i][j] = min(dist_matrix[i-1][j] + 1,
                                    dist_matrix[i-1][j-1] + sub_cost(source[i - 1], target[j - 1]),
                                     dist_matrix[i][j-1] + 1)
    print(dist_matrix)
    return dist_matrix[source_length][target_length]


def sub_cost(string1, string2):  # function checks and returns substitution cost

    if string1 == string2:
        cost = 0
    else:
        cost = 2
    return cost


print(min_edit_distance("", "execution"))




